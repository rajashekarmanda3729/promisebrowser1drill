let todosContainerEl = document.getElementById('todosContainer')

function createDOMelements(completed, title) {
    return `<div class="todo-item">
                <input type="checkbox" class="check" ${(completed) ? 'checked="checked"' : ''}/>
                <label class="todo">${title}</label>
            </div>`
}

export function crateTodo(todosDataEachUser, usersDataArr) {
    if (todosDataEachUser !== null && usersDataArr !== null) {
        let count = 0
        usersDataArr.map(user => {
            if (usersDataArr[count].id === todosDataEachUser[count].userId) {
                todosContainerEl.innerHTML += `<h1 id="heading${count}" class="name">${user.name}</h1>`
            }
            let limitElments = 0
            todosDataEachUser.map(todo => {
                if (user.id === todo.userId && limitElments < 7) {
                    document.getElementById(`heading${count}`).innerHTML += createDOMelements(todo.completed,todo.title)
                        limitElments++
                }
            })
            count++
        })
    } else {
        throw new Error('data not found to create DOM elements')
    }
}