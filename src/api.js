export function getTodosUsers() {
    let todoUsers = fetch('https://jsonplaceholder.typicode.com/users')
    return todoUsers
}

export function allUsersRequests(usersDataArr) {
    let promisesReqArr = []
    usersDataArr.map(user => promisesReqArr.push(fetch(`https://jsonplaceholder.typicode.com/todos?userId=${user.id}`)))
    return promisesReqArr
}