/*

Ensure that error handling is well tested.
If there is an error at any point, the subsequent solutions must not get executed.
Solutions without error handling will get rejected and you will be marked as having not completed this drill.
Usage of async and await is not allowed.
I am leaving selection of fonts, colours and styling on you. However make sure its usable and good looking.


Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

List 7 todos of 10 users and display them on the browser grouping them by User's Name. Example is shown below
Rekha
[ ] Make Biryani
[ ] Board on Private jet

Rajasekhar
[ ] Buy Lamborghini
[ ] Drive Bugatti Chiron
........
......

To solve this:
First fetch users.
After fetching users, use promise.all()  in parallel to fetch todos of each user.
After promise.all is settled, take the values to form the final data for rendering.
Make a template function using string literals and then use it to display the data on the browser.

*/ 